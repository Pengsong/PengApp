package peng.com.pengapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;

import peng.com.pengapp.app.login.SplashFrament;

/**
 * Created by PS on 2015/10/20.
 */
public class SplashAdapter extends FragmentStatePagerAdapter{

    private ArrayList<SplashFrament> mFragments = null;
    private ViewPager mPager = null;

    public SplashAdapter(FragmentManager fm) {
        super(fm);
        mFragments = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int i) {
        return mFragments.get(i);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    public void add(SplashFrament splashFrament) {
        splashFrament.setAdapter(this);
        mFragments.add(splashFrament);
        notifyDataSetChanged();
        mPager.setCurrentItem(getCount() - 1, true);
    }

    public void remove(int i) {
        mFragments.remove(i);
        notifyDataSetChanged();
    }

    public void remove(SplashFrament parallaxFragment) {
        mFragments.remove(parallaxFragment);
        int pos = mPager.getCurrentItem();
        notifyDataSetChanged();
        mPager.setAdapter(this);
        if (pos >= this.getCount()) {
            pos = this.getCount() - 1;
        }
        mPager.setCurrentItem(pos, true);
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public void setPager(ViewPager pager) {
        mPager = pager;
    }
}
