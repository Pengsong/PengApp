package peng.com.pengapp;

import android.app.Application;

import cn.bmob.v3.Bmob;

/**
 * Created by PS on 2015/10/20.
 */
public class MyApplication extends Application{

    private static MyApplication instance = null;

    public static MyApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        // 初始化 Bmob SDK
        // 使用时请将第二个参数Application ID替换成你在Bmob服务器端创建的Application ID
        Bmob.initialize(this, "9a2ca19bd2d7487fc517575b975f72a9");
    }
}
