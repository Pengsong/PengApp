package peng.com.pengapp.app.login;

import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import peng.com.pengapp.R;
import peng.com.pengapp.adapter.SplashAdapter;

/**
 * Created by PS on 2015/10/20.
 */
public class SplashFrament extends Fragment{

    private SplashAdapter mSplashAdapter = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragmengt_splash_screen, container, false);
        final ImageView image = (ImageView) v.findViewById(R.id.splash_image);
        image.setImageResource(getArguments().getInt("image"));
        image.post(new Runnable() {
            @Override
            public void run() {
                Matrix matrix = new Matrix();
                matrix.reset();

                float wv = image.getWidth();
                float hv = image.getHeight();

                float wi = image.getDrawable().getIntrinsicWidth();
                float hi = image.getDrawable().getIntrinsicHeight();

                float width = wv;
                float height = hv;

                if (wi / wv > hi / hv) {
                    matrix.setScale(hv / hi, hv / hi);
                    width = wi * hv / hi;
                } else {
                    matrix.setScale(wv / wi, wv / wi);
                    height= hi * wv / wi;
                }

                matrix.preTranslate((wv - width) / 2, (hv - height) / 2);
                image.setScaleType(ImageView.ScaleType.MATRIX);
                image.setImageMatrix(matrix);
            }
        });


        TextView text = (TextView)v.findViewById(R.id.cat_name);
        text.setText(getArguments().getString("name"));

        /* 这部分代码是原demo中 右上角+按钮 可以添加多张背景

        TextView more = (TextView)v.findViewById(R.id.more);

        more.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mSplashAdapter != null) {
                    mSplashAdapter.remove(SplashFrament.this);
                    mSplashAdapter.notifyDataSetChanged();
                }
                return true;
            }
        });

        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSplashAdapter != null) {
                    int select = (int) (Math.random() * 4);
                    int[] resD = {R.mipmap.bg_nina, R.mipmap.bg_niju, R.mipmap.bg_yuki, R.mipmap.bg_kero};
                    String[] resS = {"Nina", "Niju", "Yuki", "Kero"};
                    SplashFrament newP = new SplashFrament();
                    Bundle b = new Bundle();
                    b.putInt("image", resD[select]);
                    b.putString("name", resS[select]);
                    newP.setArguments(b);
                    mSplashAdapter.add(newP);
                }
            }
        });*/
        return v;
    }

    public void setAdapter(SplashAdapter catsAdapter) {
        mSplashAdapter = catsAdapter;
    }
}
