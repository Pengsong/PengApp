package peng.com.pengapp.app.login;

import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.MenuItem;

import peng.com.pengapp.R;
import peng.com.pengapp.adapter.SplashAdapter;
import peng.com.pengapp.util.SplashTransformer;

public class SplashScreenActivity extends FragmentActivity {

    private final static String TAG = "SplashScreenActivity";

    private ViewPager mPager = null;

    private SplashAdapter mAdapter = null;

    private int currentItem = 0; // 当前图片的索引号

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        mPager = (ViewPager) findViewById(R.id.splash_pager);
//        mPager.setBackgroundColor(0xFF000000);
        SplashTransformer st = new SplashTransformer((R.id.splash_image));
        //设置滑动背景边距
//        st.setBorder(20);
        //设置滑动速度
//        st.setSpeed(0.2f);
        mPager.setPageTransformer(false, st);
        // 设置一个监听器，当ViewPager中的页面改变时调用
        mPager.addOnPageChangeListener(new MyPageChangeListener());

        mAdapter = new SplashAdapter(getSupportFragmentManager());
        mAdapter.setPager(mPager); //only for this transformer

        Bundle bNina = new Bundle();
        bNina.putInt("image", R.mipmap.bg_nina);
        bNina.putString("name", "Nina");
        SplashFrament sfNina = new SplashFrament();
        sfNina.setArguments(bNina);

        Bundle bNiju = new Bundle();
        bNiju.putInt("image", R.mipmap.bg_niju);
        bNiju.putString("name", "Niju");
        SplashFrament sfNiju = new SplashFrament();
        sfNiju.setArguments(bNiju);

        Bundle bYuki = new Bundle();
        bYuki.putInt("image", R.mipmap.bg_yuki);
        bYuki.putString("name", "Yuki");
        SplashFrament sfYuki = new SplashFrament();
        sfYuki.setArguments(bYuki);

        Bundle bKero = new Bundle();
        bKero.putInt("image", R.mipmap.bg_kero);
        bKero.putString("name", "Kero");
        SplashFrament sfKero = new SplashFrament();
        sfKero.setArguments(bKero);

        mAdapter.add(sfNina);
        mAdapter.add(sfNiju);
        mAdapter.add(sfYuki);
        mAdapter.add(sfKero);
        mPager.setAdapter(mAdapter);

        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }


    /**
     * 当ViewPager中页面的状态发生改变时调用
     */
    private class MyPageChangeListener implements ViewPager.OnPageChangeListener{

        @Override
        public void onPageSelected(int position) {
            //获取当前图片索引
            currentItem = position;
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

}
